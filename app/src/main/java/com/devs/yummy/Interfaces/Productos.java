package com.devs.yummy.Interfaces;

import com.devs.yummy.Mapeo.MProductos;
import com.devs.yummy.Mapeo.Nexo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Productos {

    @GET("api/v1/search_by_date?query=mobile/")
    Call<Nexo> getProductos();

}
